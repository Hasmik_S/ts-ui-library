import React from "react";
import PropTypes from "prop-types";
import ButtonM from "@material-ui/core/Button";
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from "@material-ui/styles";
import grey from "@material-ui/core/colors/grey";
import blueGrey from "@material-ui/core/colors/blueGrey";

const theme = createMuiTheme ({
  overrides: {
    MuiButton: {
      root: {
        borderRadius: "0px",
      },
      containedPrimary: {
        color: "#fafafa",
        backgroundColor: "#212121"
      },
      containedSecondary: {
        color: "#fafafa",
        backgroundColor: "#424242"
      },
      outlinedPrimary: {
        color: "#212121",
        border: "1px solid #212121"
      },
      outlinedSecondary: {
        color: "#424242",
        border: "1px solid #424242"
      },
      textPrimary: {
        color: "#212121"
      },
      textSecondary: {
        color: "#424242"
      }
    }
  }
});


function Button(props) {
  return (
    <ThemeProvider theme={theme}>
      <ButtonM {...props} onClick={props.onClick}>{props.children}</ButtonM>
      </ThemeProvider>  
  );
}


Button.propTypes = {
  onClick: PropTypes.func,
  /**
   * The content of the button.
   */
  children: PropTypes.node.isRequired,

  /**
   * Override or extend the styles applied to the component.
   * See [CSS API](#css) below for more details.
   */
  classes: PropTypes.object.isRequired,

  /**
   * @ignore
   */
  className: PropTypes.string,

  /**
   * The color of the component. It supports those theme colors that make sense for this component.
   */
  color: PropTypes.oneOf(['default', 'inherit', 'primary', 'secondary']),

  /**
   * The component used for the root node.
   * Either a string to use a DOM element or a component.
   */
  component: PropTypes.elementType,

  /**
   * If `true`, the button will be disabled.
   */
  disabled: PropTypes.bool,

  /**
   * If `true`, the  keyboard focus ripple will be disabled.
   * `disableRipple` must also be true.
   */
  disableFocusRipple: PropTypes.bool,

  /**
   * If `true`, the ripple effect will be disabled.
   *
   * ⚠️ Without a ripple there is no styling for :focus-visible by default. Be sure
   * to highlight the element by applying separate styles with the `focusVisibleClassName`.
   */
  disableRipple: PropTypes.bool,

  /**
   * @ignore
   */
  focusVisibleClassName: PropTypes.string,

  /**
   * If `true`, the button will take up the full width of its container.
   */
  fullWidth: PropTypes.bool,

  /**
   * The URL to link to when the button is clicked.
   * If defined, an `a` element will be used as the root node.
   */
  href: PropTypes.string,

  /**
   * The size of the button.
   * `small` is equivalent to the dense button styling.
   */
  size: PropTypes.oneOf(['small', 'medium', 'large']),

  /**
   * @ignore
   */
  type: PropTypes.string,

  /**
   * The variant to use.
   */
  variant: PropTypes.oneOf(['text', 'outlined', 'contained'])
};

export { Button as default };
