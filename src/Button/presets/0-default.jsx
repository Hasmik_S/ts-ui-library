import * as React from "react";
import Button from "../Button";


export default (
  <Button uxpId="action1" 
  variant="contained"
  color="primary"
  width="99px"
  height="40px"
  border="1px solid #000000"
  margin="0px"
  background-size="cover"
  >
    Text
  </Button>
);
