import React from "react";
import PropTypes from "prop-types";
import TabM from "@material-ui/core/Tab";
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from "@material-ui/styles";
import grey from "@material-ui/core/colors/grey";



const theme = createMuiTheme ({
  overrides: {
    MuiTab: {
      root: {
        borderRadius: "12px",
        fontFamily: "Montserrat",
        fontSize: "10px",
        fontWeight: 700,
        backgroundSize: "cover",
        border: "1px solid #000000",
        minHeight: "6px",
        minWidth: "8px",
        maxWidth: "8px",
        padding: "6px 8px"
      },
      textColorPrimary: {
        color: "#212121"
      },
      textColorSecondary: {
        color:  "#757575"
      }
    }
  }
});

function Tab(props) {
  return (
  <ThemeProvider theme={theme}>
  <TabM {...props} />
  </ThemeProvider>
  );
  }
  

Tab.propTypes = {
 /**
   * This property isn't supported.
   * Use the `component` property if you need to change the children structure.
   
  children: PropTypes.node,
  */

  /**
   * Override or extend the styles applied to the component.
   * See [CSS API](#css-api) below for more details.
   */
  classes: PropTypes.object,

  /**
   * @ignore
   */
  className: PropTypes.string,

  /**
   * If `true`, the tab will be disabled.
   */
  disabled: PropTypes.bool,

  /**
   * @ignore
   */
  fullWidth: PropTypes.bool,

  /**
   * The icon element.
   */
  icon: PropTypes.node,

  /**
   * @ignore
   * For server-side rendering consideration, we let the selected tab
   * render the indicator.
  
  indicator: PropTypes.node,
   */

  /**
   * The label element.
   */
  label: PropTypes.node,

  /**
   * @ignore
   */
  onChange: PropTypes.func,

  /**
   * @ignore
   */
  onClick: PropTypes.func,

  /**
   * @ignore
   
  selected: PropTypes.bool,
  */

  /**
   * @ignore
   */
  textColor: PropTypes.oneOf([ 'primary','secondary', 'inherit']),

  /**
   * You can provide your own value. Otherwise, we fallback to the child position index.
   */
  value: PropTypes.any 
};

export { Tab as default };
