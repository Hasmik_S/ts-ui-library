import * as React from 'react';
import Icon from '../Icon';

export default (
  <Icon uxpId="1" 
  classes={{
    fontSize: { 
      small: {
        fontSize: "15px",
        width: "15px",
        height: "15px"},
        }
      }}
        >
    home
  </Icon>
);