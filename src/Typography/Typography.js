import React from "react";
import PropTypes from "prop-types";
import TypographyM from "@material-ui/core/Typography";
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from "@material-ui/styles";
import grey from "@material-ui/core/colors/grey";




const theme = createMuiTheme ({
  overrides: {
    MuiTypography: {
      colorPrimary: {
        color: grey[900]
      },
      colorSecondary: {
        color: "#757575"
      },
      colorTextPrimary: {
        color: grey[900]
      },
      colorTextSecondary: {
        color: "#757575"
      },
      subtitle1: {
        fontSize: "29px"
      },
      subtitel2: {
        fontSize: "22px"
      },
      body1: {
        fontSize: "18px"
      },
      body2: {
        fontSize: "14px"
      }
    }
  }
});


function Typography(props) {
  return (
    <ThemeProvider theme={theme}>
      <TypographyM {...props}>{props.children}</TypographyM>
    </ThemeProvider>
  );
}


Typography.propTypes = {
  /**
   * Set the text-align on the component.
   */
  align: PropTypes.oneOf([
    "inherit",
    "left",
    "center",
    "right"
  ]),

  /**
   * The content of the component.
   */
  children: PropTypes.node,

  /**
   * Override or extend the styles applied to the component.
   */
  classes: PropTypes.object,


  /**
   * @ignore
   */
  className: PropTypes.string,

  /**
   * The color of the component. It supports those theme colors that make sense for this component.
   */
  color: PropTypes.oneOf([
    "default",
    "error",
    "inherit",
    "primary",
    "secondary",
    'textPrimary', 
    'textSecondary'
  ]),

  /**
   * The component used for the root node.
   * Either a string to use a DOM element or a component.
   * By default, it maps the variant to a good default headline component.
   */
  component: PropTypes.elementType,

   /**
   * Controls the display type
   */
  display: PropTypes.oneOf(['initial', 'block', 'inline']),
 

  /**
   * We are empirically mapping the variant property to a range of different DOM element types.
   * For instance, subtitle1 to `<h6>`.
   * If you wish to change that mapping, you can provide your own.
   * Alternatively, you can use the `component` property.
   * The default mapping is the following:
   */
  headlineMapping: PropTypes.object,

    /**
   * If `true`, the text will not wrap, but instead will truncate with an ellipsis.
   */
  noWrap: PropTypes.bool,

  /**
   * Applies the theme typography styles.
   * Use `body1` as the default value with the legacy implementation and `body2` with the new one.
   */

  variantMapping: PropTypes.object,

  variant: PropTypes.oneOf([
    "h1",
    "h2",
    "h3",
    "h4",
    "h5",
    "h6",
    "subtitle1",
    "subtitle2",
    "body1",
    "body2",
  ])
};

export { Typography as default };
