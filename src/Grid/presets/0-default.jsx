import * as React from "react";
import Grid from "../Grid";
import Paper from "../../Paper/Paper";

export default (
  <Grid container spacing={24} uxpId="1">
    <Grid item xs={3} uxpId="1.1">
      <Paper uxpId="1.1.1">Dresden</Paper>
    </Grid>
    <Grid item xs={3} uxpId="1.2">
      <Paper uxpId="1.2.1">Berlin</Paper>
    </Grid>
    <Grid item xs={3} uxpId="1.3">
      <Paper uxpId="1.3.1">München</Paper>
    </Grid>
    <Grid item xs={3} uxpId="1.4">
      <Paper uxpId="1.4.1">Köln</Paper>
    </Grid>
    <Grid item xs={8} uxpId="1.5">
      <Paper uxpId="1.5.1">Bete Stadt :)</Paper>
    </Grid>
    <Grid item xs={4} uxpId="1.6">
      <Paper uxpId="1.6.1">Dresden</Paper>
    </Grid>
    <Grid item xs={8} uxpId="1.7">
      <Paper uxpId="1.7.1">Bete Leute :)</Paper>
    </Grid>
    <Grid item xs={4} uxpId="1.8">
      <Paper uxpId="1.8.1">in Dresden</Paper>
    </Grid>
    <Grid item xs={8} uxpId="1.9">
      <Paper uxpId="1.9.1">La La La </Paper>
    </Grid>
    <Grid item xs={4} uxpId="1.10">
      <Paper uxpId="1.10.1">Dresden is the best City</Paper>
    </Grid>
  </Grid>
);
