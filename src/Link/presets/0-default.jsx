import * as React from 'react';
import Link from '../Link';

export default (
  <Link 
  uxpId="1"
  underline="none"
  variant="h6"
  font-weight="bold"
>
    Text
  </Link>
);