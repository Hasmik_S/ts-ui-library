module.exports = {
  components: {
    categories: [
      {
        name: "General",
        include: [
          "src/Button/Button.js",
          "src/Icon/Icon.js", 
          "src/Typography/Typography.js",
          "src/Input/Input.js",
          "src/Link/Link.js",
          "src/Tab/Tab.js",
          "src/Tabs/Tabs.js",
          "src/Grid/Grid.js",
          "src/Paper/Paper.js",
          "src/FormLabel/FormLabel.js"
        ]
      },
    ]
  },
  name: "TS-TEST-Library"
};

/**
 * Currently not supported:
 * 'src/Menu/Menu.js',
 * 'src/ExpansionPanel/ExpansionPanel.js',
 * 'src/ExpansionPanelActions/ExpansionPanelActions.js',
 * 'src/ExpansionPanelDetails/ExpansionPanelDetails.js',
 * 'src/ExpansionPanelSummary/ExpansionPanelSummary.js'
 * 'src/Snackbar/Snackbar.js'
 * 'src/SnackbarContent/SnackbarContent.js'
 */
